<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Currency extends Model
{
    use HasFactory;
    protected $table = 'currency';


    private static $date_start = '';
    private static $date_end = '';
//    const CBR_URL_DAILY = 'https://www.cbr-xml-daily.ru/daily_utf8.xml';
    const CBR_URL_DAILY = 'https://www.cbr-xml-daily.ru/daily_json.js';
    const CBR_URL_DYNAMIC = 'http://www.cbr.ru/scripts/XML_dynamic.asp';

    protected $_data = [];

    public function __construct()
    {
        if (self::$date_start === '') {
            self::$date_start = date('d/m/Y', time() - 172800);
        }
        if (self::$date_end === '') {
            self::$date_end = date('d/m/Y');
        }
        parent::__construct();
    }

    public function cbrGetData($type = 'daily')
    {


        /*Daily*/
        if ($type == 'daily') {
            $link = self::CBR_URL_DAILY;
            // Получаем текущие курсы валют в rss-формате с сайта www.cbr.ru
            $content = $this->get_content($link);
            $courses = json_decode($content,true);

            foreach ($courses['Valute'] as $cur) {
                $model = new Currency();
                $model->currencyID =$cur['ID'];
                $model->numCode =$cur['NumCode'];
                $model->charCode =$cur['CharCode'];
                $model->name =$cur['Name'];
                $model->value =$cur['Value'];
                $model->created_at =$courses['Date'];
                $model->save();
            }
        }
        /*Daily*/

        /*Dynamic*/
        if ($type == 'dynamic') {
            # Базовый URL скрипта на cbr.ru
            $link = self::CBR_URL_DYNAMIC;
            $scripturl = $link;
            # Начальная дата для запроса  (сегодня - 2 дня)
            $date_1 = self::$date_start;
            # Конечная дата (чтобы учитывать завтра добавьте параметр time()+86400)
            $date_2 = self::$date_end;
            # Код валюты в архиве данных cbr.ru
            $currency_code = 'R01235';
            # URL для запроса данных
            $requrl = "{$scripturl}?date_req1={$date_1}&date_req2={$date_2}&VAL_NM_RQ={$currency_code}";
            $doc = file($requrl);
            $doc = implode($doc, '');
        }
        /*Dynamic*/
    }

    function get_content($link)
    {
        $fd = fopen($link, "r");
        $text = "";
        if (!$fd) echo "err";
        else {
            while (!feof($fd)) $text .= fgets($fd, 4096);
        }
        fclose($fd);
        return $text;
    }

}
