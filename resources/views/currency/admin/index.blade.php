@extends('layouts.app')

@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('head')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1">
                <div class="card-body">
                    <div class="table-responsive">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm data-table">
                            <thead>
                            <tr class="success">
                                <th>СurrencyID</th>
                                <th>Name</th>
                                <th>СharCode</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($gridData as $data)
                                <tr>
                                    <td>{{$data->getAttribute('currencyID')}}</td>
                                    <td>{{$data->getAttribute('name')}}</td>
                                    <td>{{$data->getAttribute('charCode')}}</td>
                                    <td>{{$data->getAttribute('value')}} RUB</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection
