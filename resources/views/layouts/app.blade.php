<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@hasSection('template_title')@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
        <meta name="description" content="">
        <meta name="author" content="Jeremy Kenedy">
        <link rel="shortcut icon" href="/favicon.ico">

        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        {{-- Fonts --}}
        @yield('template_linked_fonts')

        {{-- Styles --}}
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

        @yield('template_linked_css')

        <style type="text/css">
            @yield('template_fastload_css')

            @if (Auth::User() && (Auth::User()->profile) && (Auth::User()->profile->avatar_status == 0))
                .user-avatar-nav {
                    background: url({{ Gravatar::get(Auth::user()->email) }}) 50% 50% no-repeat;
                    background-size: auto 100%;
                }
            @endif

        </style>

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>

        @if (Auth::User() && (Auth::User()->profile) && $theme->link != null && $theme->link != 'null')
            <link rel="stylesheet" type="text/css" href="{{ $theme->link }}">
        @endif

        @yield('head')
        @include('scripts.ga-analytics')
    </head>
    <body>
        <div id="app">

            @include('partials.nav')

            <main class="py-4">

                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            @include('partials.form-status')
                        </div>
                    </div>
                </div>

                @yield('content')

            </main>

        </div>

        {{-- Scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>

        @if(config('settings.googleMapsAPIStatus'))
            {!! HTML::script('//maps.googleapis.com/maps/api/js?key='.config("settings.googleMapsAPIKey").'&libraries=places&dummy=.js', array('type' => 'text/javascript')) !!}
        @endif

        @yield('footer_scripts')
        <script src="assets/mockjax/jquery.mockjax.js"></script>
        <script src="assets/momentjs/moment.min.js"></script>
        <!-- select2 -->
        <link href="assets/select2/select2.css" rel="stylesheet">
        <script src="assets/select2/select2.js"></script>


        <!-- bootstrap -->
{{--        <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">--}}
{{--        <link href="assets/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">--}}
{{--        <script src="assets/bootstrap/js/bootstrap.js"></script>--}}
        {{----}}
                            <link href="assets/bootstrap300/css/bootstrap.css" rel="stylesheet">
                        <script src="assets/bootstrap300/js/bootstrap.js"></script>

        <!-- bootstrap-datetimepicker -->
        <link href="assets/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet">
        <script src="assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

        <!-- x-editable (bootstrap 3) -->
        <link href="assets/x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
        <script src="assets/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>

        <!-- select2 bootstrap -->
        <link href="assets/select2/select2-bootstrap.css" rel="stylesheet">

        <!-- typeaheadjs -->
        <link href="assets/x-editable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css" rel="stylesheet">
        <script src="assets/x-editable/inputs-ext/typeaheadjs/lib/typeahead.js"></script>
        <script src="assets/x-editable/inputs-ext/typeaheadjs/typeaheadjs.js"></script>

        <!-- wysihtml5 -->
        <link href="assets/x-editable/inputs-ext/wysihtml5/bootstrap-wysihtml5-0.0.3/bootstrap-wysihtml5-0.0.3.css" rel="stylesheet">
        <script src="assets/x-editable/inputs-ext/wysihtml5/bootstrap-wysihtml5-0.0.3/wysihtml5-0.3.0.min.js"></script>
        <script src="assets/x-editable/inputs-ext/wysihtml5/bootstrap-wysihtml5-0.0.3/bootstrap-wysihtml5-0.0.3.min.js"></script>
        <script src="assets/x-editable/inputs-ext/wysihtml5/wysihtml5-0.0.3.js"></script>
        <!---->
        <script>
            var f = 'bootstrap3';
        </script>
        <link href="assets/x-editable/inputs-ext/address/address.css" rel="stylesheet">
        <script src="assets/x-editable/inputs-ext/address/address.js"></script>
        <script src="assets/demo-mock.js"></script>
        <script src="assets/demo.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var speedCanvas = document.getElementById("speedChart");

                Chart.defaults.global.defaultFontFamily = "Lato";
                Chart.defaults.global.defaultFontSize = 18;

                var dataFirst = {
                    label: "USD",
                    data: [27.30, 27.22, 26.30, 29.30, 30.01, 27.30, 27.30,27.30,27.30,27.30],
                    lineTension: 0,
                    fill: false,
                    borderColor: 'red'
                };

                var dataSecond = {
                    label: "EUR",
                    data: [34.30, 32.22, 30.30, 32.30, 31.01, 29.30, 27.30,27.30,27.30,27.30],
                    lineTension: 0,
                    fill: false,
                    borderColor: 'blue'
                };

                var speedData = {
                    labels: ["1d", "2d", "3d", "4d", "5d", "6d", "7d","8d","9d","10d"],
                    datasets: [dataFirst, dataSecond]
                };

                var chartOptions = {
                    legend: {
                        display: true,
                        position: 'top',
                        labels: {
                            boxWidth: 80,
                            fontColor: 'black'
                        }
                    }
                };

                var lineChart = new Chart(speedCanvas, {
                    type: 'line',
                    data: speedData,
                    options: chartOptions
                });
            });
        </script>
    </body>
</html>
