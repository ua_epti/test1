@extends('layouts.app')

@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('head')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1">

                <div style="width: 80%; margin: auto;">
                    <canvas id="speedChart" width="600" height="400"></canvas>


                    <footer class="footer" style="clear: both; padding-top: 10px">
                        <hr>
                    </footer>

                </div>
            </div>
        </div>
    </div>
@endsection
